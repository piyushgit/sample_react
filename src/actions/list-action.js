import * as actions from "./type";

export function setError(error) {
    return {
        type: actions.CURRENT_LIST_ERROR,
        error: error
    };
}

export function getCurrenctList() {
    return {
        type: actions.CURRENT_LIST
    };
}

export function setCurrentList(list) {
    return {
        type: actions.SET_CURRENT_LIST,
        list: list
    };
}