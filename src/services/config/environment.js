const baseUrl = process.env.NODE_ENV === 'development' ? '' : '/';
export { baseUrl };
