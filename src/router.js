import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { baseUrl } from './services/config/environment';
import App from './components/App';

class Router extends Component {
    render() {
        return (
            <BrowserRouter basename={baseUrl}>
                <App />
            </BrowserRouter>
        );
    }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(Router);
