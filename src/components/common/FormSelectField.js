import React from 'react';
import PropTypes from 'prop-types';

class FormSelectField extends React.Component
{
    constructor(props) {
    super(props);
    this.state = {
        alertClass: ''
    };
    this.onSelect = this.onSelect.bind(this);
  }

    onSelect(e)
    {
        const target = e.currentTarget;
        if(target.value == '')
        {
            this.setState({alertClass: 'is-invalid'});
        }
        else
        {
            this.setState({alertClass: ''});
        }
        this.props.callBackSelect(target.value); 
    } 

    render(){  
        let i = 1;
        const {operation, dayData, yearData, selectedValue} = this.props;
    
        let task = (operation=='dayBirth')?dayData:(operation=='yearBirth')?yearData:this.props.monthData;

        let monthOption = task.map(data => 
          <option key={i++} value={data.value} >{data.label}</option>
        );
        return(
            <fieldset className="form-group">
                <select className={`${this.props.className} ${this.state.alertClass} selectStyle`} defaultValue={selectedValue} name={operation} onChange={this.onSelect} >
                   {monthOption}
                </select>
            </fieldset>
        );
    }
}

let dayData = [{label:'Day', value:''}];
let changeValue;
for (let i = 1; i <= 31; i++) {
    changeValue = (i<10)?`0${i}`:i;
  dayData.push({label: i, value:changeValue});
}

let yearData = [{label:'Year', value:''}];
for(let i=1910; i<=2018; i++ ){
    yearData.push({label: i, value: i});
}

FormSelectField.defaultProps = {
    dayData,
    yearData,
    monthData: [
        {
            label: 'Month',
            value:  ''
        },
        {
            label: 'January',
            value:  '01'
        },
        {
            label: 'February',
            value:  '02'
        },
        {
            label: 'March',
            value:  '03'
        },
        {
            label: 'April',
            value: '04'
        },
        {
            label: 'May',
            value:  '05'
        },
        {
            label: 'June',
            value:  '06'
        },
        {
            label: 'July',
            value:  '07'
        },
        {
            label: 'August',
            value:  '08'
        },
        {
            label: 'September',
            value:  '09'
        },
        {
            label: 'October',
            value:  '10'
        },
        {
            label: 'November',
            value:  '11'
        },
        {
            label: 'December',
            value:  '12'
        }
    ]
};

FormSelectField.propTypes = {
    callBackSelect: PropTypes.func.isRequired,
    className: PropTypes.string,
    monthData: PropTypes.array.isRequired,
    selectedValue: PropTypes.string,
    operation: PropTypes.string.isRequired,
    dayData: PropTypes.array.isRequired,
    yearData: PropTypes.array.isRequired
};

export default FormSelectField ;