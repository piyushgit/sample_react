import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import {
    FieldFeedbacks,
    FormGroup,
    FormControlLabel,
    FormControlInput
} from 'react-form-with-constraints/lib/Bootstrap4';

class Listing extends React.Component {
    constructor(props) {
        super(props);
        this.showList = this.showList.bind(this);
    }

    listItem(list, index) {
        return (
            <tr key={index}>
                <td className="deal-content">{list.first_name}</td>
                <td>{list.last_name}</td>
                <td>{list.email}</td>
                <td>{list.status}</td>
            </tr>
        );
    }

    showList(list) {
        return (list.map((item, index) => {
            return this.listItem(item, index);
        }));
    }

    showTable() {
        return (
            <div className="col-lg">
                <div className="row">
                    <div className="col-md-12 col-md-offset-1">
                        <div className="panel panel-default panel-table">
                            <div className="panel-body">
                                <div className="row" >
                                    <div className="table-responsive tab-res">
                                        <table className="table deal table-striped text-center table-hover table-bordered table-list">
                                            <thead>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.showList(this.props.list)}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <section className="g-py-100">
                <div className="container">
                    <div className="text-center g-mb-60 mb-4">
                        <h2 className="h4">List Tabel</h2>
                    </div>
                    {this.showTable()}
                </div>
            </section>
        );
    }
}

Listing.propTypes = {
    list: PropTypes.array
};

export default Listing;