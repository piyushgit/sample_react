import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state =
            {
                authUser: '',
                dropdownOpen: false,
                navStatus: true
            };
    }



    render() {

        let authToken = localStorage.getItem("userAuthToken");
        const isAuthenticated = (authToken ? true : false);

        return (
            <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onClick={this.onSideNav}>
                    <span className="navbar-toggler-icon"></span>
                </button>
                <NavLink className="navbar-brand" to="/">List</NavLink>
            </nav>
        );

    }
}

Header.propTypes = {
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(Header);