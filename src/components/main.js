import React from "react";
import { Switch, Route } from "react-router-dom";
import ListingPage from "./listing/ListingPage";

const Main = () => (
    <main className="p50">
        <Switch>
            <Route path="/" component={ListingPage} />
        </Switch>
    </main>
);

export default Main;
