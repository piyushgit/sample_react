export default (state = [], action) => {
    let newStatus = '';
    switch (action.type) {
        case 'SET_CURRENT_LIST':
            return [...state, Object.assign({}, action.list)]
        default:
            return state;
    }
};

