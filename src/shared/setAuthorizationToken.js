import axios from 'axios';

import config from '../data/env';
const baseUrl = config[0].userApiUrl;

export const setAuthorizationToken = (token) => {
  axios.get(`${baseUrl}/users/me`, {
    headers: { 'Authorization': `Bearer ${token}` }
  })
    .then(response => {
      //console.log(response);
    })
    .catch((error) => {
      //console.log(error.response);
    });

};