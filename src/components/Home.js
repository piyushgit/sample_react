// import React from "react";
// import axios from "axios";
// import {Redirect} from "react-router-dom";
// import ProfileSideNav from './profile/ProfileSideNav';
// import {getUserFeed} from '../utils/apiCall';

// class Home extends React.Component 
// {
//     constructor()
//     {
//         super();
//         this.state = {
//             userName: ''
//         };
//     }

//     componentWillMount() 
//     {
//         let userAuthToken = localStorage.getItem("userAuthToken");
//         if(userAuthToken)
//         {
//             getUserFeed(userAuthToken).then(
//         (res)=>{if(res.data.error){localStorage.removeItem('userAuthToken'); this.setState({redirect: true});}else{this.setState({userName: res.data.first_name+' '+res.data.last_name});}});
//         }
//     }

//     render()
//     {
//         let user = JSON.parse(localStorage.getItem('loggedUser'));
//         if(this.state.redirect)
//         {
//             return <Redirect to="/login" />;
//         }
//         return (
//             <div className="container">
//                 <div className="row">
//                     <div className="col-md-3 col-sm-12 col-lg-3">
//                         <ProfileSideNav />
//                     </div>
//                     <div className="col-md-9 col-sm-12 col-lg-9">
//                         <div className="row">
//                             <div className="col-sm-12 ml-sm-auto col-md-12 pt-3">
//                                 <div className="page-wrapper">
//                                     <div className="container-fluid">
//                                         <div className="row">
//                                             <div className="col-sm-12 col-md-12 ">
//                                                 <hr className="one"/>
//                                                 <h1>Welcome {user.firstName+' '+user.lastName} to the User Management system !</h1>
//                                             </div>
//                                         </div>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         );
//     }
// }

// export default Home;

