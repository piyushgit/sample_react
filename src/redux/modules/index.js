import { combineReducers } from 'redux';
import list from './listReducer';

const allReducers = combineReducers(
  {
    list
  }
);

export default allReducers;