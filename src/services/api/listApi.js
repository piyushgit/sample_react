import config from '../config';
import fetchJson from './utils/fetchJson';
import * as listActions from '../../actions/list-action';

const fakeListResponse = [{
    "id": 159,
    "title": "YALLOTRADE AB LOOKING FOR FRONT-END DEVELOPER",
    "discount": "10%",
    "logo": "demo_logo_1.png",
    "detail": "You will work in close cooperation with the team and have an influence on the technology stack and how the team solves UX and GUI related development. You will be part of the whole process from planning to implementation. Keeping up with the latest technology and frameworks is expected, to secure that the company product is developed and adapted in the best possible way.",
    "detail_url": "desuvit.com",
    "contact_person": "Jordbro sam",
    "status": "Active",
    "sort_order": 464
},
{
    "id": 159,
    "title": "BUTIKSSÄLJARE, MALL OF SCANDINAVIA",
    "discount": "20%",
    "logo": "demo_logo_2.png",
    "detail": "You will work in close cooperation with the team and have an influence on the technology stack and how the team solves UX and GUI related development. You will be part of the whole process from planning to implementation. Keeping up with the latest technology and frameworks is expected, to secure that the company product is developed and adapted in the best possible way.",
    "detail_url": "facebook.com",
    "contact_person": "Jordbro dfasfasdf",
    "status": "In Active",
    "sort_order": 464
},
{
    "id": 159,
    "title": "SENIOR KEY ACCOUNT MANAGER",
    "discount": "30%",
    "logo": "demo_logo_1.png",
    "detail": "You will work in close cooperation with the team and have an influence on the technology stack and how the team solves UX and GUI related development. You will be part of the whole process from planning to implementation. Keeping up with the latest technology and frameworks is expected, to secure that the company product is developed and adapted in the best possible way.",
    "detail_url": "hotmail.com",
    "contact_person": "Jordbro",
    "status": "Active",
    "sort_order": 464
}];

const fakeListError = {
    status: 404,
    message: ""
};

const realApi = false;
const fakeErrorResponse = false;

function accessResponse(ms, object, props) {
    props.dispatch(listActions.setCurrentList(object));

    return new Promise((resolve, reject) => {
        setTimeout(() => (fakeErrorResponse) ? reject(object) : resolve(object), ms);
    });
}

export function fetchList(props) {
    if (realApi) {
        return fetchJson(`${config.apiBaseUrl}/deals`).catch(error => {
            throw new Error('Failed to get deal list');
        });
    } else {
        if (fakeErrorResponse)
            return accessResponse(4000, fakeListError, props);
        else
            return accessResponse(4000, fakeListResponse, props);
    }
}
