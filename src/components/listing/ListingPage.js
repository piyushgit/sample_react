import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as listActions from "../../actions/list-action";
import Listing from './Listing';
import * as Api from "../../services/api/listApi";

class ListingPage extends React.Component {

    componentDidMount() {
        //this.props.dispatch(listActions.setCurrentList());
        const response = Api.fetchList(this.props);
        console.log('list', this.props.list);
    }

    render() {
        return (
            <div className="row py-4 justify-content-md-center">
                <div className="col-lg-8 col-md-8 col-sm-12">
                    <Listing {...this.props} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    list: state.list
});

export default connect(mapStateToProps)(ListingPage);